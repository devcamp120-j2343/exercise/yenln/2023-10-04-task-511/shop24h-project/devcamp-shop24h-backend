//B1: Import Thư viện express
//import { Express } from "express";
const express = require("express");

//Import thư viện mongoose
var mongoose = require('mongoose');

//b2 khởi tạo app express
const app = new express();

//b3: khai báo cỗng để chạy api
const port = 8000;

mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Pizza365")
    .then(() => console.log("Connected to Mongo Successfully"))
    .catch(error => handleError(error));

const productTypeRouter = require("./app/routes/ProductTypeoRouter");
const productRouter = require("./app/routes/ProductRouter");
const customerRouter = require("./app/routes/CustomerRouter");
const orderRouter = require("./app/routes/OrderRouter");
const orderDetailRouter = require("./app/routes/OrderDetailRouter");


//cấu hình để sử dụng json
app.use(express.json());

//Middleware
//Middleware console log ra thời gian hiện tại 
app.use((req, res, next) => {
    console.log("Thời gian hiện tại:", new Date());

    next();
})

app.use("/productType", productTypeRouter);
app.use("/products", productRouter);
app.use("/customers", customerRouter);
app.use("/orders", orderRouter);
app.use("/orderDetail", orderDetailRouter);


//b4: star app
app.listen(port, () => {
    console.log(`app listening on port ${port}`);
})
