const mongoose = require("mongoose");

const customerModel = require("../model/CustomerModel");

const createCustomer = async (req, res) => {
    const {
        fullName,
        phone,
        email,
        address,
        city,
        country,
        orders,
    } = req.body;

    if (!fullName) {
        return res.status(400).json({
            message: "fullName is invalid",
        });
    }
    if (!phone) {
        return res.status(400).json({
            message: "phone is invalid",
        });
    }
    if (!email) {
        return res.status(400).json({
            message: "email is invalid",
        });
    }
    if (!typeof address == "string") {
        return res.status(400).json({
            message: "address is invalid",
        });
    }
    if (!typeof city == "string") {
        return res.status(400).json({
            message: "city is invalid",
        });
    }

    if (!typeof country == "string") {
        return res.status(400).json({
            message: "country is invalid",
        });
    }

    const newCustomer = {
        _id: new mongoose.Types.ObjectId(),
        fullName,
        phone,
        email,
        address,
        city,
        country,
        orders,
    }

    customerModel.create(newCustomer)
        .then((data) => {
            return res.status(201).json({
                status: "Create new Customer sucessfully",
                data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}

const getAllCustomer = async (req, res) => {

    customerModel.find()
        .then((data) => {
            return res.status(201).json({
                status: "Get all Customers sucessfully",
                data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}

const getCustomerByID = async (req, res) => {
    const customerId = req.params.customerId;

    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "customerId is invalid!"
        })
    }

    customerModel.findById(customerId)
        .then((data) => {
            return res.status(201).json({
                status: `Get Customer with id ${customerId} sucessfully`,
                data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}

const updateCustomer = async (req, res) => {
    const customerId = req.params.customerId;
    const {
        fullName,
        phone,
        email,
        address,
        city,
        country,
        orders,
    } = req.body;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "customerId is invalid!"
        })
    }

    if (!fullName) {
        return res.status(400).json({
            message: "fullName is invalid",
        });
    }
    if (!phone) {
        return res.status(400).json({
            message: "phone is invalid",
        });
    }
    if (!email) {
        return res.status(400).json({
            message: "email is invalid",
        });
    }
    if (!typeof address == "string") {
        return res.status(400).json({
            message: "address is invalid",
        });
    }
    if (!typeof city == "string") {
        return res.status(400).json({
            message: "city is invalid",
        });
    }

    if (!typeof country == "string") {
        return res.status(400).json({
            message: "country is invalid",
        });
    }


    try {
        const updateCustomer = {
        }
        if (fullName) {
            updateCustomer.fullName = fullName
        }
        if (phone) {
            updateCustomer.phone = phone
        }
        if (email) {
            updateCustomer.email = email
        }
        if (address) {
            updateCustomer.address = address
        }
        if (city) {
            updateCustomer.city = city
        }
        if (country) {
            updateCustomer.country = country
        }

        const result = await customerModel.findByIdAndUpdate(
            customerId,
            updateCustomer
        );
        if (result) {
            return res.status(200).json({
                status: "Update Customer sucessfully",
                data: updateCustomer
            })
        } else {
            return res.status(404).json({
                status: "Not found any Customer"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

const deleteCustomer = async (req, res) => {
    const customerId = req.params.customerId;

    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "customerId is invalid!"
        })
    }
    try {
        const deleteCustomer = await customerModel.findByIdAndDelete(customerId);

        if (deleteCustomer) {
            return res.status(200).json({
                status: `Delete Customer ${customerId} sucessfully`,
                data: deleteCustomer
            })
        } else {
            return res.status(404).json({
                status: "Not found any Customer"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }

}

module.exports = {
    createCustomer,
    getAllCustomer,
    getCustomerByID,
    updateCustomer,
    deleteCustomer
}