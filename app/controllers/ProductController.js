const mongoose = require("mongoose");

const ProductModel = require("../model/ProductModel");
const productTypeModel = require('../model/ProductTypeModel');

const createProduct = async (req, res) => {
    const {
        name,
        description,
        type,
        imageUrl,
        buyPrice,
        promotionPrice,
        amount
    } = req.body;

    if (!name) {
        return res.status(400).json({
            message: "name is invalid",
        });
    }
    if (!mongoose.Types.ObjectId.isValid(type)) {
        return res.status(400).json({
            status: "Bad request",
            message: "type is invalid!"
        })
    }
    if (!imageUrl) {
        return res.status(400).json({
            message: "imageUrl is invalid",
        });
    }
    if (!buyPrice || isNaN(buyPrice)) {
        return res.status(400).json({
            message: "buyPrice is invalid",
        });
    }
    if (!promotionPrice || isNaN(promotionPrice)) {
        return res.status(400).json({
            message: "promotionPrice is invalid",
        });
    }
    if (amount < 0) {
        return res.status(400).json({
            message: "amount is invalid",
        });
    }

    try {
        const newProduct = {
            _id: new mongoose.Types.ObjectId(),
            name,
            description,
            type,
            imageUrl,
            buyPrice,
            promotionPrice,
            amount
        }

        const createProduct = await ProductModel.create(newProduct);

        const updateProductType = await productTypeModel.findByIdAndUpdate(type, {
            $push: { type: createProduct }
        })

        if (!updateProductType) {
            return res.status(404).json({
                message: "Co loi xay ra",
            });
        }
        return res.status(201).json({
            message: "Create order successfully",
            productType: updateProductType,
            data: createProduct
        })

    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })

    }


}

const getAllProduct = async (req, res) => {

    ProductModel.find()
        .then((data) => {
            return res.status(201).json({
                status: "Get all Product sucessfully",
                data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}

const getProductByID = async (req, res) => {
    const productId = req.params.productId;

    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "productId is invalid!"
        })
    }

    ProductModel.findById(productId)
        .then((data) => {
            return res.status(201).json({
                status: `Get Product with id ${productId} sucessfully`,
                data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}

const updateProduct = async (req, res) => {
    const productId = req.params.productId;
    const {
        name,
        description,
        type,
        imageUrl,
        buyPrice,
        promotionPrice,
        amount
    } = req.body;

    //B2: kiểm tra dữ liệu
    if (!name) {
        return res.status(400).json({
            message: "name is invalid",
        });
    }
    if (!mongoose.Types.ObjectId.isValid(type)) {
        return res.status(400).json({
            status: "Bad request",
            message: "type is invalid!"
        })
    }
    if (!imageUrl) {
        return res.status(400).json({
            message: "imageUrl is invalid",
        });
    }
    if (!buyPrice || isNaN(buyPrice)) {
        return res.status(400).json({
            message: "buyPrice is invalid",
        });
    }
    if (!promotionPrice || isNaN(promotionPrice)) {
        return res.status(400).json({
            message: "promotionPrice is invalid",
        });
    }
    if (amount < 0) {
        return res.status(400).json({
            message: "amount is invalid",
        });
    }

    try {
        const updateProduct = {
        }
        if (name) {
            updateProduct.name = name
        }
        if (description) {
            updateProduct.description = description
        }
        if (type) {
            updateProduct.type = type;
        }
        if (imageUrl) {
            updateProduct.imageUrl = imageUrl
        }
        if (buyPrice) {
            updateProduct.buyPrice = buyPrice
        }
        if (promotionPrice) {
            updateProduct.promotionPrice = promotionPrice
        }
        if (amount) {
            updateProduct.amount = amount
        }

        const result = await ProductModel.findByIdAndUpdate(
            productId,
            updateProduct
        );

        if (result) {
            return res.status(200).json({
                status: "Update Product sucessfully",
                data: updateProduct
            })
        } else {
            return res.status(404).json({
                status: "Not found any Product"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

const deleteProduct = async (req, res) => {
    const productId = req.params.productId;

    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "productId is invalid!"
        })
    }
    try {
        const deleteProduct = await ProductModel.findByIdAndDelete(productId);

        if (deleteProduct) {
            return res.status(200).json({
                status: `Delete Product ${productId} sucessfully`,
                data: deleteProduct
            })
        } else {
            return res.status(404).json({
                status: "Not found any Product"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }

}

module.exports = {
    createProduct,
    getAllProduct,
    getProductByID,
    updateProduct,
    deleteProduct
}