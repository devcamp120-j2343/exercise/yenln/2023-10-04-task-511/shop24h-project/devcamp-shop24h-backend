const mongoose = require("mongoose");

const orderModel = require("../model/OrderModel");

const createOrder = async (req, res) => {
    const {
        orderDate,
        shippedDate,
        note,
        orderDetails,
        cost
    } = req.body;

    if (!orderDate) {
        return res.status(400).json({
            message: "orderDate is invalid",
        });
    }
    if (!typeof note == "String") {
        return res.status(400).json({
            message: "note is invalid",
        });
    }
    if (isNaN(cost) || cost < 0) {
        return res.status(400).json({
            message: "cost is invalid",
        });
    }

    const newOrder = {
        _id: new mongoose.Types.ObjectId(),
        orderDate,
        shippedDate,
        note,
        orderDetails,
        cost
    }

    orderModel.create(newOrder)
        .then((data) => {
            return res.status(201).json({
                status: "Create new Order sucessfully",
                data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}

const getAllOrder = async (req, res) => {

    orderModel.find()
        .then((data) => {
            return res.status(201).json({
                status: "Get all Orders sucessfully",
                data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}

const getAllOrderOfCustomer = async (req, res) => {
    const orderId = req.params.orderId;
    const customerId = req.params.customerId;

    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "customerId is invalid!"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "orderId is invalid!"
        })
    }

    orderModel.findById(customerId)
        .then((data) => {
            return res.status(201).json({
                status: `Get Order with customerId ${customerId} sucessfully`,
                data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}

const getOrderByID = async (req, res) => {
    const orderId = req.params.orderId;

    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "orderId is invalid!"
        })
    }

    orderModel.findById(orderId)
        .then((data) => {
            return res.status(201).json({
                status: `Get Order with id ${orderId} sucessfully`,
                data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}

const updateOrder = async (req, res) => {
    const orderId = req.params.orderId;
    const {
        orderDate,
        shippedDate,
        note,
        orderDetails,
        cost
    } = req.body;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "orderId is invalid!"
        })
    }

    if (!orderDate) {
        return res.status(400).json({
            message: "orderDate is invalid",
        });
    }
    if (!typeof note == "String") {
        return res.status(400).json({
            message: "note is invalid",
        });
    }
    if (isNaN(cost) || cost < 0) {
        return res.status(400).json({
            message: "cost is invalid",
        });
    }



    try {
        const updateOrder = {
        }
        if (orderDate) {
            updateOrder.orderDate = orderDate
        }
        if (shippedDate) {
            updateOrder.shippedDate = shippedDate
        }
        if (note) {
            updateOrder.note = note
        }
        if (orderDetails) {
            updateOrder.orderDetails = orderDetails
        }
        if (cost) {
            updateOrder.cost = cost
        }

        const result = await orderModel.findByIdAndUpdate(
            orderId,
            updateOrder
        );
        if (result) {
            return res.status(200).json({
                status: "Update Order sucessfully",
                data: updateOrder
            })
        } else {
            return res.status(404).json({
                status: "Not found any Order"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

const deleteOrder = async (req, res) => {
    const orderId = req.params.orderId;

    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "orderId is invalid!"
        })
    }
    try {
        const deleteOrder = await orderModel.findByIdAndDelete(orderId);

        if (deleteOrder) {
            return res.status(200).json({
                status: `Delete Order ${orderId} sucessfully`,
                data: deleteOrder
            })
        } else {
            return res.status(404).json({
                status: "Not found any Order"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }

}

module.exports = {
    createOrder,
    getAllOrder,
    getAllOrderOfCustomer,
    getOrderByID,
    updateOrder,
    deleteOrder
}