const mongoose = require("mongoose");

const productTypeModel = require("../model/ProductTypeModel");

const CreateProductType = async (req, res) => {
    const {
        name,
        description
    } = req.body;

    if (!name) {
        return res.status(400).json({
            message: "name is invalid",
        });
    }

    const newProductType = {
        _id: new mongoose.Types.ObjectId(),
        name,
        description
    }

    productTypeModel.create(newProductType)
        .then((data) => {
            return res.status(201).json({
                status: "Create new productType sucessfully",
                data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}

const GetAllProductType = async (req, res) => {

    productTypeModel.find()
        .then((data) => {
            return res.status(201).json({
                status: "Get all productType sucessfully",
                data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}

const GetProductTypeByID = async (req, res) => {
    const userId = req.params.userId;

    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "userId is invalid!"
        })
    }

    productTypeModel.findById(userId)
        .then((data) => {
            return res.status(201).json({
                status: `Get productType with id ${userId} sucessfully`,
                data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}

const UpdateProductType = async (req, res) => {
    const userId = req.params.userId;
    const {
        name,
        description
    } = req.body;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "userId is invalid!"
        })
    }

    if (!name) {
        return res.status(400).json({
            message: "name is invalid",
        });
    }

    try {
        const updateProductType = {
        }
        if (name) {
            updateProductType.name = name
        }
        if (description) {
            updateProductType.description = description
        }
        const result = await productTypeModel.findByIdAndUpdate(
            userId,
            updateProductType
        );
        if (result) {
            return res.status(200).json({
                status: "Update ProductType sucessfully",
                data: updateProductType
            })
        } else {
            return res.status(404).json({
                status: "Not found any ProductType"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

const DeleteProductType = async (req, res) => {
    const userId = req.params.userId;

    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "userId is invalid!"
        })
    }
    try {
        const deleteProductType = await productTypeModel.findByIdAndDelete(userId);

        if(deleteProductType){
            return res.status(200).json({
                status: `Delete ProductType ${userId} sucessfully`,
                data: deleteProductType
            })
        } else{
            return res.status(404).json({
                status: "Not found any ProductType"
            })
        }    
    } catch (error){
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }

}

module.exports = {
    CreateProductType,
    GetAllProductType,
    GetProductTypeByID,
    UpdateProductType,
    DeleteProductType
}