const mongoose = require("mongoose");

const orderDetailModel = require("../model/OrderDetailModel");

const createOrderDetailOfOrder = async (req, res) => {
    const {
        product,
        quantity,
        orderId
    } = req.body;

    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "product is invalid!"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(product)) {
        return res.status(400).json({
            status: "Bad request",
            message: "product is invalid!"
        })
    }
    if (isNaN(quantity)) {
        return res.status(400).json({
            message: "quantity is invalid",
        });
    }
    const newOrderDetail = {
        _id: new mongoose.Types.ObjectId(),
        product,
        quantity,
        orderId
    }

    orderDetailModel.create(newOrderDetail)
        .then((data) => {
            return res.status(201).json({
                status: "Create new OrderDetail sucessfully",
                data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}

const getAllOrderDetailOfOrder = async (req, res) => {

    orderDetailModel.find()
        .then((data) => {
            return res.status(201).json({
                status: "Get all OrderDetails sucessfully",
                data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}

const getOrderDetailById = async (req, res) => {
    const orderDetailId = req.params.orderDetailId;

    if (!mongoose.Types.ObjectId.isValid(orderDetailId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "orderDetailId is invalid!"
        })
    }

    orderDetailModel.findById(orderDetailId)
        .then((data) => {
            return res.status(201).json({
                status: `Get OrderDetail with id ${orderDetailId} sucessfully`,
                data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}

const updateOrderDetail = async (req, res) => {
    const orderDetailId = req.params.orderDetailId;
    const {
        product,
        quantity,
    } = req.body;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderDetailId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "orderDetailId is invalid!"
        })
    }

    if (!mongoose.Types.ObjectId.isValid(product)) {
        return res.status(400).json({
            status: "Bad request",
            message: "product is invalid!"
        })
    }
    if (isNaN(quantity)) {
        return res.status(400).json({
            message: "quantity is invalid",
        });
    }


    try {
        const updateOrderDetail = {
        }
        if (product) {
            updateOrderDetail.product = product
        }
        if (quantity) {
            updateOrderDetail.quantity = quantity
        }

        const result = await orderDetailModel.findByIdAndUpdate(
            orderDetailId,
            updateOrderDetail,
            {new: true}
        );
        if (result) {
            return res.status(200).json({
                status: "Update OrderDetail sucessfully",
                data: updateOrderDetail
            })
        } else {
            return res.status(404).json({
                status: "Not found any OrderDetail"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

const deleteOrderDetail = async (req, res) => {
    const orderDetailId = req.params.orderDetailId;

    if (!mongoose.Types.ObjectId.isValid(orderDetailId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "orderDetailId is invalid!"
        })
    }
    try {
        const deleteOrderDetail = await orderDetailModel.findByIdAndDelete(orderDetailId);

        if (deleteOrderDetail) {
            return res.status(200).json({
                status: `Delete OrderDetail ${orderDetailId} sucessfully`,
                data: deleteOrderDetail
            })
        } else {
            return res.status(404).json({
                status: "Not found any OrderDetail"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }

}

module.exports = {
    createOrderDetailOfOrder,
    getAllOrderDetailOfOrder,
    getOrderDetailById,
    updateOrderDetail,
    deleteOrderDetail
}