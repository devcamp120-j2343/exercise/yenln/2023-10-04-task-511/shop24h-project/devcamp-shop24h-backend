//B1 khai báo thư viện mongoose
const mongoose = require("mongoose")

//B2 Khai báo class Schema
const Schema = mongoose.Schema;

//B3 Khởi tạo schema với các thuộc tính của collection
const customerSchema = new Schema ({
    _id: mongoose.Types.ObjectId,
    fullName: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true,
        unique: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    address: {
        type: String,
        default: "",
    },
    city: {
        type: String,
        default: "",
    },
    country: {
        type: String,
        default: "",
    },
    orders: [
        {
            type: mongoose.Types.ObjectId,
            ref: "Order",
        }
    ]
})

//B4: biên dịch schema thành model

module.exports = mongoose.model("customer", customerSchema)