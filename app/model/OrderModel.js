//B1 khai báo thư viện mongoose
const mongoose = require("mongoose")

//B2 Khai báo class Schema
const Schema = mongoose.Schema;

//B3 Khởi tạo schema với các thuộc tính của collection
const orderSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    orderDate: {
        type: Date,
        default: () => new Date(+new Date() + 7 * 24 * 60 * 60 * 1000),
    },
    shippedDate: {
        type: Date,
    },
    note: {
        type: String,
    },
    orderDetails: [
        {
            type: mongoose.Types.ObjectId,
            ref: "OrderDetail",
        }
    ],
    cost: {
        type: Number,
        default: 0,
    }
})

//B4: biên dịch schema thành model

module.exports = mongoose.model("order", orderSchema)