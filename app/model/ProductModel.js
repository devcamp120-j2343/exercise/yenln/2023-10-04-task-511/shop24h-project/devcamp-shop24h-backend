//B1 khai báo thư viện mongoose
const mongoose = require("mongoose")

//B2 Khai báo class Schema
const Schema = mongoose.Schema;

//B3 Khởi tạo schema với các thuộc tính của collection
const productSchema = new Schema ({
    _id: mongoose.Types.ObjectId,
    name: {
        type: String,
        unique: true,
        required: true
    },
    description: {
        type: String,
    },
    type: [
        {
            type: mongoose.Types.ObjectId,
            ref: "productType",
            required: true
        }
    ],
    imageUrl: {
        type: String,
        required: true
    },
    buyPrice: {
        type: Number,
        required: true 
    },
    promotionPrice: {
        type: Number,
        required: true 
    },
    amount: {
        type: Number,
        default: 0,
    }
})

//B4: biên dịch schema thành model

module.exports = mongoose.model("product", productSchema)