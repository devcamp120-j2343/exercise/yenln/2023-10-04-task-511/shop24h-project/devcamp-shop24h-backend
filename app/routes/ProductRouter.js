//import { Express } from "express";
const express = require("express");

const productModel = require("../model/ProductModel");
const productsMiddlewares = require("../middlewares/ProductMiddlewares");
const productController = require("../controllers/ProductController");

const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL course: ", req.url);

    next();
})

router.post("/",productsMiddlewares.createProductsMiddlewares, productController.createProduct);

router.get("/",productsMiddlewares.getAllProductsMiddlewares, productController.getAllProduct);

router.get("/:productId",productsMiddlewares.getDetailProductsMiddlewares, productController.getProductByID);

router.put("/:productId",productsMiddlewares.updateProductsMiddlewares, productController.updateProduct);

router.delete("/:productId",productsMiddlewares.deleteProductsMiddlewares, productController.deleteProduct);

module.exports = router;