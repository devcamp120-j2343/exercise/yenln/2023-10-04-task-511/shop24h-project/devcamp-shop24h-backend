//import { Express } from "express";
const express = require("express");

const orderDetailModel = require("../model/OrderDetailModel");
const orderDetailsMiddlewares = require("../middlewares/OrderDetailMiddlewares");
const orderDetailController = require("../controllers/OrderDetailController");

const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL course: ", req.url);

    next();
})

router.post("/:orderId",orderDetailsMiddlewares.createOrderDetailsMiddlewares, orderDetailController.createOrderDetailOfOrder);

router.get("/",orderDetailsMiddlewares.getAllOrderDetailsMiddlewares, orderDetailController.getAllOrderDetailOfOrder);

router.get("/:orderDetailId",orderDetailsMiddlewares.getDetailOrderDetailsMiddlewares, orderDetailController.getOrderDetailById);

router.put("/:orderDetailId",orderDetailsMiddlewares.updateOrderDetailsMiddlewares, orderDetailController.updateOrderDetail);

router.delete("/:orderDetailId",orderDetailsMiddlewares.deleteOrderDetailsMiddlewares, orderDetailController.deleteOrderDetail);

module.exports = router;