//import { Express } from "express";
const express = require("express");

const productTypeModel = require("../model/ProductTypeModel");
const productTypesMiddlewares = require("../middlewares/ProductTypeMiddleware");
const productTypeController = require("../controllers/ProductTypeController");

const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL course: ", req.url);

    next();
})

router.post("/",productTypesMiddlewares.createProductTypesMiddlewares, productTypeController.CreateProductType);

router.get("/",productTypesMiddlewares.getAllProductTypesMiddlewares, productTypeController.GetAllProductType);

router.get("/:userId",productTypesMiddlewares.getDetailProductTypesMiddlewares, productTypeController.GetProductTypeByID);

router.put("/:userId",productTypesMiddlewares.updateProductTypesMiddlewares, productTypeController.UpdateProductType);

router.delete("/:userId",productTypesMiddlewares.deleteProductTypesMiddlewares, productTypeController.DeleteProductType);

module.exports = router;