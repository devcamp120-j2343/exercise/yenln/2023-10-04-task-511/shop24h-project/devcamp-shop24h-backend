//import { Express } from "express";
const express = require("express");

const orderModel = require("../model/OrderModel");
const ordersMiddlewares = require("../middlewares/OrderMiddlewares");
const orderController = require("../controllers/OrderController");

const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL course: ", req.url);

    next();
})

router.post("/:customerId",ordersMiddlewares.createOrdersMiddlewares, orderController.createOrder);

router.get("/",ordersMiddlewares.getAllOrdersMiddlewares, orderController.getAllOrder);

router.get("/:customerId/:orderId",ordersMiddlewares.getAllOrdersOfCustomerMiddlewares, orderController.getAllOrderOfCustomer);

router.put("/:orderId",ordersMiddlewares.updateOrdersMiddlewares, orderController.updateOrder);

router.delete("/:orderId",ordersMiddlewares.deleteOrdersMiddlewares, orderController.deleteOrder);

module.exports = router;