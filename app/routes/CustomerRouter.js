//import { Express } from "express";
const express = require("express");

const customerModel = require("../model/CustomerModel");
const customersMiddlewares = require("../middlewares/CustomerMiddlewares");
const customerController = require("../controllers/CustomerController");

const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL course: ", req.url);

    next();
})

router.post("/",customersMiddlewares.createCustomersMiddlewares, customerController.createCustomer);

router.get("/",customersMiddlewares.getAllCustomersMiddlewares, customerController.getAllCustomer);

router.get("/:customerId",customersMiddlewares.getDetailCustomersMiddlewares, customerController.getCustomerByID);

router.put("/:customerId",customersMiddlewares.updateCustomersMiddlewares, customerController.updateCustomer);

router.delete("/:customerId",customersMiddlewares.deleteCustomersMiddlewares, customerController.deleteCustomer);

module.exports = router;