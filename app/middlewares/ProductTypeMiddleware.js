const getAllProductTypesMiddlewares = (req, res, next) => {
    console.log("Get all ProductTypes Middleware");

    next();
}

const createProductTypesMiddlewares = (req, res, next) => {
    console.log("Create ProductTypes Middleware");

    next();
}

const getDetailProductTypesMiddlewares = (req, res, next) => {
    console.log("Get detail ProductType Middleware");

    next();
}

const updateProductTypesMiddlewares = (req, res, next) => {
    console.log("Update ProductTypes Middleware");

    next();
}

const deleteProductTypesMiddlewares = (req, res, next) => {
    console.log("Delete ProductTypes Middleware");

    next();
}

module.exports = {
    getAllProductTypesMiddlewares,
    createProductTypesMiddlewares, 
    getDetailProductTypesMiddlewares,
    updateProductTypesMiddlewares,
    deleteProductTypesMiddlewares
}