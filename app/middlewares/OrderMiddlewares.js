const getAllOrdersMiddlewares = (req, res, next) => {
    console.log("Get all Orders Middleware");

    next();
}

const createOrdersMiddlewares = (req, res, next) => {
    console.log("Create Orders Middleware");

    next();
}

const getAllOrdersOfCustomerMiddlewares = (req, res, next) => {
    console.log("Get All order of customer Middleware");

    next();
}

const updateOrdersMiddlewares = (req, res, next) => {
    console.log("Update Orders Middleware");

    next();
}

const deleteOrdersMiddlewares = (req, res, next) => {
    console.log("Delete Orders Middleware");

    next();
}

module.exports = {
    getAllOrdersMiddlewares,
    createOrdersMiddlewares, 
    getAllOrdersOfCustomerMiddlewares,
    updateOrdersMiddlewares,
    deleteOrdersMiddlewares
}