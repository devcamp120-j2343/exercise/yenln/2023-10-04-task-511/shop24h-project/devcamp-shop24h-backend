const getAllOrderDetailsMiddlewares = (req, res, next) => {
    console.log("Get all OrderDetails Middleware");

    next();
}

const createOrderDetailsMiddlewares = (req, res, next) => {
    console.log("Create OrderDetails Middleware");

    next();
}

const getDetailOrderDetailsMiddlewares = (req, res, next) => {
    console.log("Get detail OrderDetail Middleware");

    next();
}

const updateOrderDetailsMiddlewares = (req, res, next) => {
    console.log("Update OrderDetails Middleware");

    next();
}

const deleteOrderDetailsMiddlewares = (req, res, next) => {
    console.log("Delete OrderDetails Middleware");

    next();
}

module.exports = {
    getAllOrderDetailsMiddlewares,
    createOrderDetailsMiddlewares, 
    getDetailOrderDetailsMiddlewares,
    updateOrderDetailsMiddlewares,
    deleteOrderDetailsMiddlewares
}