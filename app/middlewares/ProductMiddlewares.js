const getAllProductsMiddlewares = (req, res, next) => {
    console.log("Get all Products Middleware");

    next();
}

const createProductsMiddlewares = (req, res, next) => {
    console.log("Create Products Middleware");

    next();
}

const getDetailProductsMiddlewares = (req, res, next) => {
    console.log("Get detail Product Middleware");

    next();
}

const updateProductsMiddlewares = (req, res, next) => {
    console.log("Update Products Middleware");

    next();
}

const deleteProductsMiddlewares = (req, res, next) => {
    console.log("Delete Products Middleware");

    next();
}

module.exports = {
    getAllProductsMiddlewares,
    createProductsMiddlewares, 
    getDetailProductsMiddlewares,
    updateProductsMiddlewares,
    deleteProductsMiddlewares
}