const getAllCustomersMiddlewares = (req, res, next) => {
    console.log("Get all Customers Middleware");

    next();
}

const createCustomersMiddlewares = (req, res, next) => {
    console.log("Create Customers Middleware");

    next();
}

const getDetailCustomersMiddlewares = (req, res, next) => {
    console.log("Get detail Customer Middleware");

    next();
}

const updateCustomersMiddlewares = (req, res, next) => {
    console.log("Update Customers Middleware");

    next();
}

const deleteCustomersMiddlewares = (req, res, next) => {
    console.log("Delete Customers Middleware");

    next();
}

module.exports = {
    getAllCustomersMiddlewares,
    createCustomersMiddlewares, 
    getDetailCustomersMiddlewares,
    updateCustomersMiddlewares,
    deleteCustomersMiddlewares
}